# Art2Script

Scripts for sending keyswitches without using the output section of LogicPro articulation set

See Wiki section for more info:  [WIKI HERE](https://gitlab.com/dewdman42/art2script/-/wikis/home)
