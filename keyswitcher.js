/*****************************************************************
 * Keyswitcher.js
 * 
 * v1.4
 * 
 * This Scripter script will send keyswitches and CC switches on
 * per-articulationID basis.  This can be used to replace the
 * OUTPUT section of an articulation set for various reasons 
 * that this might be needed.
 * 
 * This contains some example keyswitches, edit to your needs
 * 
 * More info: https://gitlab.com/dewdman42/art2script/-/tree/main
 * 
 ****************************************************************/

var MAXPORTS=48;

/********************************************************
 * Initialize ART array
 *
 * ART[port][channel][artid]
 *
 *******************************************************/
 
function Initialize() {

    // Example

    /*****************************************************
     *  port:1  channel:1  instrument: KH Violins 1St
     *****************************************************/
    
    ART[1][1][1] = {name:"Sustain", ks:["C-1","D-1","C0"]};       // sustain optional channel
    ART[1][1][2] = {name:"Staccato", ks:["C#0","D#0","E0","F0"]}; // staccato
    ART[1][1][3] = {name:"CC Example", ks:["C0",[58,5]]};         // use case with CC58=5
    ART[1][1][4] = {name:"Channelizing example", channel:5};     // just channelizing
    ART[1][1][5] = {name:"Channelizing with switches", channel:6, ks:["C1",[58,5]]};  // combo example
    // etc
    
}


/***************************************************
 ************** DO NOT EDIT BELOW HERE *************
 ***************************************************/

var ART = new Array(MAXPORTS+1);
for(let port=0; port<ART.length; port++) {
    ART[port] = new Array(17);
    for(let chan=0; chan<ART[port].length; chan++) {
        ART[port][chan] = [];
    }
}

var lastArtID = new Array(MAXPORTS+1);
for(let port=0; port<ART.length; port++) {
    lastArtID[port] = new Array(17);
    for(let chan=0; chan<lastArtID[port].length; chan++) {
        lastArtID[port][chan] = 0;
    }
}

    
function HandleMIDI(event) {

    // Everything other then Note
    if( !(event instanceof Note)) {
        event.send();
        return;
    }
    
    // default articulationID 
    if(event.articulationID == undefined) {
        event.articulationID = 0;
    }
    
    // default midi port
    if(event.port == undefined) event.port = 1;

    let artic = ART[event.port][event.channel][event.articulationID];
    if( event.articulationID > 0 && artic == undefined ) {
        Trace(`WARNING, missing entry: ART[${event.port}][${event.channel}][${event.articulationID}]`);
        event.send();
        return;
    }

    // if Art entry includes channel, then channelize
    if( artic.channel != undefined) {
        event.channel = artic.channel;
    }

    if(event instanceof NoteOn) {
        // Track last articulationID
        let last = lastArtID[event.port][event.channel];
        lastArtID[event.port][event.channel] = event.articulationID;    

        // If same artID as last note or NoteOff, skip keyswitching
        if(event.articulationID == last || event instanceof NoteOff 
                || (event instanceof NoteOn && event.velocity < 1)) {
            event.send();
            return;
        }

        // send keyswitches
        let ksList = artic.ks;
        if(ksList == undefined) {
            // no keyswitches defined
            event.send();
            return;
        }

        for(let k=0; k<ksList.length; k++) {
    
            if(Array.isArray(ksList[k])) {
                sendCC(ksList[k], event);
            }
            else {
                sendSwitch(ksList[k], event);
            }
        }
    }
     
    // send actual noteon or Noteoff    
    event.send();
}


// Function to send a single keyswitch

var ks = new NoteOn;
function sendSwitch(pitchName, evt) {
        ks.pitch = MIDI.noteNumber(pitchName);
        ks.channel = evt.channel;
        ks.port = evt.port;
        ks.velocity = 100;
        ks.send();
        ks.velocity = 0;
        ks.send();   
}

// Function to send a single CC switch

var cc = new ControlChange;
function sendCC(ccArray, evt) {
    cc.number = ccArray[0];
    cc.value = ccArray[1];
    cc.channel = evt.channel;
    cc.port = evt.port;
    cc.send();
}


