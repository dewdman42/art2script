/**************************************************************
 * lanekeyswitcher.js
 * 
 * v1.3
 * 
 * Example Script that convert automation lane to keyswitches
 *
 * Note this example only works for a single midi channel,
 * Otherwise seperate automation lanes will need to be defined
 * for each midi channel in the ART[] array
 * 
 * More info: https://gitlab.com/dewdman42/art2script/-/tree/main
 * 
 **************************************************************/


var MAXPORTS=48;

/********************************************************
 * Initialize ART array
 *
 * ART[port][channel][artid]
 *
 *******************************************************/

function Initialize() {

    // Example


    /*****************************************************
     *  port:1  channel:1  instrument: KH Violins 1St
     *****************************************************/

    ART[1][1][1] = {name:"Accent Ff", ks:["E0"]}; 
    ART[1][1][2] = {name:"Accent Mf", ks:["D#0"]}; 
    ART[1][1][3] = {name:"Accent Pp", ks:["D0"]}; 
    ART[1][1][4] = {name:"Adagio", ks:["F0"]}; 
    ART[1][1][5] = {name:"Harmonics", ks:["C1"]}; 
    ART[1][1][6] = {name:"Long", ks:["C#0"]}; 
    ART[1][1][7] = {name:"Pizzicato", ks:["A#0"]}; 
    ART[1][1][8] = {name:"Pizzicato Bartok", ks:["B0"]}; 
    ART[1][1][9] = {name:"Short", ks:["F#0"]}; 
    ART[1][1][10] = {name:"Tremolo", ks:["G0"]}; 
    ART[1][1][11] = {name:"Trill Maj2", ks:["A0"]}; 
    ART[1][1][12] = {name:"Trill Min2", ks:["G#0"]}; 

    /* initLane */
    initLane( 1, 1, "KH Violins 1St" );

    /*****************************************************
     *  port:1  channel:5  instrument: KH Violins 1St
     *****************************************************/

    ART[1][5][1] = {name:"Accent Ff", ks:["E0"]}; 
    ART[1][5][2] = {name:"Accent Mf", ks:["D#0"]}; 
    ART[1][5][3] = {name:"Accent Pp", ks:["D0"]}; 
    ART[1][5][4] = {name:"Adagio", ks:["F0"]}; 
    ART[1][5][5] = {name:"Harmonics", ks:["C1"]}; 
    ART[1][5][6] = {name:"Long", ks:["C#0"]}; 
    ART[1][5][7] = {name:"Pizzicato", ks:["A#0"]}; 
    ART[1][5][8] = {name:"Pizzicato Bartok", ks:["B0"]}; 
    ART[1][5][9] = {name:"Short", ks:["F#0"]}; 
    ART[1][5][10] = {name:"Tremolo", ks:["G0"]}; 
    ART[1][5][11] = {name:"Trill Maj2", ks:["A0"]}; 
    ART[1][5][12] = {name:"Trill Min2", ks:["G#0"]}; 

    /* initLane */
    initLane( 1, 5, "KH Violins 1St" );


    // etc
}

/***************************************************
 ************** DO NOT EDIT BELOW HERE *************
 ***************************************************/

function initLane(port, channel, name ) {

    // build names array for automation
    let names = ["Reset"];
    let a = ART[port][channel];
    if(a != undefined) {
        for(let i=1;i<a.length;i++) {
            if(a[i] != undefined) {
               names.push(a[i].name);
            }
        }
    }

    PluginParameters.push({
        type: "menu",
        name: name,
        defaultValue: 0,
        hidden: true,
        disableAutomation: false,
        valueStrings: names
    });

    LANEIDS[port][channel] = PluginParameters.length-1;
}




var ART = new Array(MAXPORTS+1);
for(let port=0; port<ART.length; port++) {
    ART[port] = new Array(17);
    for(let chan=0; chan<ART[port].length; chan++) {
        ART[port][chan] = [];
    }
}

var lastArtID = new Array(MAXPORTS+1);
for(let port=0; port<lastArtID.length; port++) {
    lastArtID[port] = new Array(17);
    for(let chan=0; chan<lastArtID[port].length; chan++) {
        lastArtID[port][chan] = 0;
    }
}

var LANEIDS = new Array(MAXPORTS+1);
for(let port=0; port<LANEIDS.length; port++) {
    LANEIDS[port] = new Array(17);
    for(let chan=0; chan<LANEIDS[port].length; chan++) {
        LANEIDS[port][chan] = 0
    }
}

var PluginParameters = [];

function HandleMIDI(event) {

    // Everything other then NoteOn
    if( !(event instanceof NoteOn)) {
        event.send();
        return;
    }
      
    // default the port
    if(event.port == undefined) event.port = 1;

    // lookup values per port/channel
    let laneID = LANEIDS[event.port][event.channel];
    let currentArtID = GuiParameters.get(laneID);
    let artObj = ART[event.port][event.channel][currentArtID];
    let last = lastArtID[event.port][event.channel];

    // If same artID as last note, skip keyswitching
    if(currentArtID == last) {
        event.send();
        return;
    }

    // check for missing entry
    if (artObj == undefined || artObj.ks == undefined) {
            
        event.send();
        Trace("WARNING: articulation entry not found: ART[" + event.port
                + "][" + event.channel + "][" + event.articulationID + "]");
        return;
    }
    
    // remember the current ID 
    lastArtID[event.port][event.channel] = currentArtID;
    
    // send keyswitches
    let ksList = artObj.ks;
    
    for(let k=0; k<ksList.length; k++) {
    
        if(Array.isArray(ksList[k])) {
            sendCC(ksList[k], event);
        }
        else {
            sendSwitch(ksList[k], event);
        }
    }
     
    // send actual note    
    event.send();
}

// Function to send a single keyswitch

var ks = new NoteOn;
function sendSwitch(pitchName, evt) {
        ks.pitch = MIDI.noteNumber(pitchName);
        ks.channel = evt.channel;
        ks.port = evt.port;
        ks.velocity = 100;
        ks.send();
        ks.velocity = 0;
        ks.send();   
}

// Function to send a single CC switch

var cc = new ControlChange;
function sendCC(ccArray, evt) {
    cc.number = ccArray[0];
    cc.value = ccArray[1];
    cc.channel = evt.channel;
    cc.port = evt.port;
    cc.send();
}


// cached parameters
var GuiParameters = {
    data: [],
    set: function(id, val) {
        this.data[id] = val;
    },
    get: function(id) {
        if(this.data[id] == undefined) {
            this.data[id] = GetParameter(id);
        }
        return this.data[id];
    }
};
function ParameterChanged(id, val) {
    GuiParameters.set(id, val);
}

